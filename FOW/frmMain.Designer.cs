namespace FOW
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tsNew = new System.Windows.Forms.ToolStripButton();
            this.tsOpen = new System.Windows.Forms.ToolStripButton();
            this.tsSave = new System.Windows.Forms.ToolStripButton();
            this.tsGenerate = new System.Windows.Forms.ToolStripButton();
            this.tsExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsAbout = new System.Windows.Forms.ToolStripButton();
            this.tsHelp = new System.Windows.Forms.ToolStripButton();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stylesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabFOW = new System.Windows.Forms.TabControl();
            this.tabImages = new System.Windows.Forms.TabPage();
            this.ImagesList = new System.Windows.Forms.DataGridView();
            this.colImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.colImgtitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colImgTxt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFullPathWithExt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImagesBottomPanel = new System.Windows.Forms.Panel();
            this.btnSelectStylesTab = new System.Windows.Forms.Button();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnAddFolder = new System.Windows.Forms.Button();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.ImagesTopPanel = new System.Windows.Forms.Panel();
            this.txtAlbumDesc = new System.Windows.Forms.TextBox();
            this.txtAlbumTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabStyles = new System.Windows.Forms.TabPage();
            this.StylesTopPanel = new System.Windows.Forms.Panel();
            this.PreviewBox = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStylesDescription = new System.Windows.Forms.TextBox();
            this.cmbStyles = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.StylesBottomPanel = new System.Windows.Forms.Panel();
            this.btnSelectImages = new System.Windows.Forms.Button();
            this.btnGenerateAlbum = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.toolStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabFOW.SuspendLayout();
            this.tabImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImagesList)).BeginInit();
            this.ImagesBottomPanel.SuspendLayout();
            this.ImagesTopPanel.SuspendLayout();
            this.tabStyles.SuspendLayout();
            this.StylesTopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreviewBox)).BeginInit();
            this.StylesBottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsNew,
            this.tsOpen,
            this.tsSave,
            this.tsGenerate,
            this.tsExit,
            this.toolStripSeparator1,
            this.tsAbout,
            this.tsHelp});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(613, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            // 
            // tsNew
            // 
            this.tsNew.Image = global::FOW.Properties.Resources._new;
            this.tsNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsNew.Name = "tsNew";
            this.tsNew.Size = new System.Drawing.Size(48, 22);
            this.tsNew.Text = "New";
            this.tsNew.ToolTipText = "Create a new album";
            this.tsNew.Click += new System.EventHandler(this.tsNew_Click);
            // 
            // tsOpen
            // 
            this.tsOpen.Image = global::FOW.Properties.Resources.open;
            this.tsOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsOpen.Name = "tsOpen";
            this.tsOpen.Size = new System.Drawing.Size(53, 22);
            this.tsOpen.Text = "Open";
            this.tsOpen.ToolTipText = "Open an album";
            this.tsOpen.Click += new System.EventHandler(this.tsOpen_Click);
            // 
            // tsSave
            // 
            this.tsSave.Image = global::FOW.Properties.Resources.save;
            this.tsSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSave.Name = "tsSave";
            this.tsSave.Size = new System.Drawing.Size(51, 22);
            this.tsSave.Text = "Save";
            this.tsSave.ToolTipText = "Save the current album";
            this.tsSave.Click += new System.EventHandler(this.tsSave_Click);
            // 
            // tsGenerate
            // 
            this.tsGenerate.Image = global::FOW.Properties.Resources.web_txfr;
            this.tsGenerate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsGenerate.Name = "tsGenerate";
            this.tsGenerate.Size = new System.Drawing.Size(72, 22);
            this.tsGenerate.Text = "Generate";
            this.tsGenerate.Click += new System.EventHandler(this.tsGenerate_Click);
            // 
            // tsExit
            // 
            this.tsExit.Image = global::FOW.Properties.Resources.close;
            this.tsExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsExit.Name = "tsExit";
            this.tsExit.Size = new System.Drawing.Size(45, 22);
            this.tsExit.Text = "Exit";
            this.tsExit.Click += new System.EventHandler(this.tsExit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsAbout
            // 
            this.tsAbout.Image = global::FOW.Properties.Resources.about;
            this.tsAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsAbout.Name = "tsAbout";
            this.tsAbout.Size = new System.Drawing.Size(56, 22);
            this.tsAbout.Text = "About";
            this.tsAbout.Click += new System.EventHandler(this.tsAbout_Click);
            // 
            // tsHelp
            // 
            this.tsHelp.Image = global::FOW.Properties.Resources.helpbubble;
            this.tsHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsHelp.Name = "tsHelp";
            this.tsHelp.Size = new System.Drawing.Size(48, 22);
            this.tsHelp.Text = "Help";
            this.tsHelp.Click += new System.EventHandler(this.tsHelp_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(613, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator3,
            this.generateToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "&Album";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(166, 6);
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.generateToolStripMenuItem.Text = "&Generate";
            this.generateToolStripMenuItem.Click += new System.EventHandler(this.generateToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(166, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imagesToolStripMenuItem,
            this.stylesToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // imagesToolStripMenuItem
            // 
            this.imagesToolStripMenuItem.Name = "imagesToolStripMenuItem";
            this.imagesToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.imagesToolStripMenuItem.Text = "&Images";
            this.imagesToolStripMenuItem.Click += new System.EventHandler(this.imagesToolStripMenuItem_Click);
            // 
            // stylesToolStripMenuItem
            // 
            this.stylesToolStripMenuItem.Name = "stylesToolStripMenuItem";
            this.stylesToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.stylesToolStripMenuItem.Text = "&Styles";
            this.stylesToolStripMenuItem.Click += new System.EventHandler(this.stylesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.toolStripStatusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 433);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(613, 22);
            this.statusBar.SizingGrip = false;
            this.statusBar.TabIndex = 3;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabFOW);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(613, 384);
            this.panel1.TabIndex = 4;
            // 
            // tabFOW
            // 
            this.tabFOW.Controls.Add(this.tabImages);
            this.tabFOW.Controls.Add(this.tabStyles);
            this.tabFOW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabFOW.Location = new System.Drawing.Point(0, 0);
            this.tabFOW.Name = "tabFOW";
            this.tabFOW.SelectedIndex = 0;
            this.tabFOW.ShowToolTips = true;
            this.tabFOW.Size = new System.Drawing.Size(613, 384);
            this.tabFOW.TabIndex = 1;
            // 
            // tabImages
            // 
            this.tabImages.Controls.Add(this.ImagesList);
            this.tabImages.Controls.Add(this.ImagesBottomPanel);
            this.tabImages.Controls.Add(this.ImagesTopPanel);
            this.tabImages.Location = new System.Drawing.Point(4, 22);
            this.tabImages.Name = "tabImages";
            this.tabImages.Padding = new System.Windows.Forms.Padding(3);
            this.tabImages.Size = new System.Drawing.Size(605, 358);
            this.tabImages.TabIndex = 0;
            this.tabImages.Text = "Images";
            this.tabImages.ToolTipText = "Select your images";
            this.tabImages.UseVisualStyleBackColor = true;
            // 
            // ImagesList
            // 
            this.ImagesList.AllowUserToAddRows = false;
            this.ImagesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ImagesList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colImage,
            this.colImgtitle,
            this.colImgTxt,
            this.colFullPathWithExt});
            this.ImagesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ImagesList.Location = new System.Drawing.Point(3, 93);
            this.ImagesList.Name = "ImagesList";
            this.ImagesList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ImagesList.Size = new System.Drawing.Size(599, 226);
            this.ImagesList.TabIndex = 3;
            // 
            // colImage
            // 
            this.colImage.HeaderText = "Image";
            this.colImage.Name = "colImage";
            this.colImage.ReadOnly = true;
            // 
            // colImgtitle
            // 
            this.colImgtitle.HeaderText = "Image Title";
            this.colImgtitle.Name = "colImgtitle";
            this.colImgtitle.Width = 140;
            // 
            // colImgTxt
            // 
            this.colImgTxt.HeaderText = "Image Text";
            this.colImgTxt.Name = "colImgTxt";
            this.colImgTxt.Width = 280;
            // 
            // colFullPathWithExt
            // 
            this.colFullPathWithExt.HeaderText = "Fullpath";
            this.colFullPathWithExt.Name = "colFullPathWithExt";
            this.colFullPathWithExt.Visible = false;
            // 
            // ImagesBottomPanel
            // 
            this.ImagesBottomPanel.Controls.Add(this.btnSelectStylesTab);
            this.ImagesBottomPanel.Controls.Add(this.btnClearAll);
            this.ImagesBottomPanel.Controls.Add(this.btnAddFolder);
            this.ImagesBottomPanel.Controls.Add(this.btnAddImage);
            this.ImagesBottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ImagesBottomPanel.Location = new System.Drawing.Point(3, 319);
            this.ImagesBottomPanel.Name = "ImagesBottomPanel";
            this.ImagesBottomPanel.Size = new System.Drawing.Size(599, 36);
            this.ImagesBottomPanel.TabIndex = 1;
            // 
            // btnSelectStylesTab
            // 
            this.btnSelectStylesTab.Location = new System.Drawing.Point(485, 6);
            this.btnSelectStylesTab.Name = "btnSelectStylesTab";
            this.btnSelectStylesTab.Size = new System.Drawing.Size(109, 25);
            this.btnSelectStylesTab.TabIndex = 7;
            this.btnSelectStylesTab.Text = "Select &Style >>";
            this.toolTip.SetToolTip(this.btnSelectStylesTab, "Move on to select a style");
            this.btnSelectStylesTab.UseVisualStyleBackColor = true;
            this.btnSelectStylesTab.Click += new System.EventHandler(this.btnSelectStylesTab_Click);
            // 
            // btnClearAll
            // 
            this.btnClearAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnClearAll.Image = global::FOW.Properties.Resources.trash_16x16;
            this.btnClearAll.Location = new System.Drawing.Point(331, 6);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(77, 23);
            this.btnClearAll.TabIndex = 6;
            this.btnClearAll.Text = "Clear List";
            this.btnClearAll.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnClearAll.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnClearAll, "Clear all images");
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // btnAddFolder
            // 
            this.btnAddFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAddFolder.Image = global::FOW.Properties.Resources.folder__add__16x16;
            this.btnAddFolder.Location = new System.Drawing.Point(230, 6);
            this.btnAddFolder.Name = "btnAddFolder";
            this.btnAddFolder.Size = new System.Drawing.Size(82, 23);
            this.btnAddFolder.TabIndex = 5;
            this.btnAddFolder.Text = "Add Folder";
            this.btnAddFolder.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnAddFolder.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnAddFolder, "Add all images from a folder");
            this.btnAddFolder.UseVisualStyleBackColor = true;
            this.btnAddFolder.Click += new System.EventHandler(this.btnAddFolder_Click);
            // 
            // btnAddImage
            // 
            this.btnAddImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAddImage.Image = global::FOW.Properties.Resources.image__add__16x16;
            this.btnAddImage.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAddImage.Location = new System.Drawing.Point(124, 6);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(87, 23);
            this.btnAddImage.TabIndex = 4;
            this.btnAddImage.Text = "Add Images";
            this.btnAddImage.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnAddImage.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip.SetToolTip(this.btnAddImage, "Add images");
            this.btnAddImage.UseVisualStyleBackColor = true;
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // ImagesTopPanel
            // 
            this.ImagesTopPanel.BackColor = System.Drawing.SystemColors.Control;
            this.ImagesTopPanel.Controls.Add(this.txtAlbumDesc);
            this.ImagesTopPanel.Controls.Add(this.txtAlbumTitle);
            this.ImagesTopPanel.Controls.Add(this.label2);
            this.ImagesTopPanel.Controls.Add(this.label1);
            this.ImagesTopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ImagesTopPanel.Location = new System.Drawing.Point(3, 3);
            this.ImagesTopPanel.Name = "ImagesTopPanel";
            this.ImagesTopPanel.Size = new System.Drawing.Size(599, 90);
            this.ImagesTopPanel.TabIndex = 0;
            // 
            // txtAlbumDesc
            // 
            this.txtAlbumDesc.Location = new System.Drawing.Point(166, 40);
            this.txtAlbumDesc.Multiline = true;
            this.txtAlbumDesc.Name = "txtAlbumDesc";
            this.txtAlbumDesc.Size = new System.Drawing.Size(349, 41);
            this.txtAlbumDesc.TabIndex = 2;
            this.toolTip.SetToolTip(this.txtAlbumDesc, "Describe your album");
            // 
            // txtAlbumTitle
            // 
            this.txtAlbumTitle.Location = new System.Drawing.Point(166, 9);
            this.txtAlbumTitle.Name = "txtAlbumTitle";
            this.txtAlbumTitle.Size = new System.Drawing.Size(212, 20);
            this.txtAlbumTitle.TabIndex = 1;
            this.toolTip.SetToolTip(this.txtAlbumTitle, "Enter album title");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Album Description:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Album Title:";
            // 
            // tabStyles
            // 
            this.tabStyles.Controls.Add(this.StylesTopPanel);
            this.tabStyles.Controls.Add(this.StylesBottomPanel);
            this.tabStyles.Location = new System.Drawing.Point(4, 22);
            this.tabStyles.Name = "tabStyles";
            this.tabStyles.Padding = new System.Windows.Forms.Padding(3);
            this.tabStyles.Size = new System.Drawing.Size(605, 358);
            this.tabStyles.TabIndex = 1;
            this.tabStyles.Text = "Styles";
            this.tabStyles.ToolTipText = "Choose the styles";
            this.tabStyles.UseVisualStyleBackColor = true;
            // 
            // StylesTopPanel
            // 
            this.StylesTopPanel.Controls.Add(this.PreviewBox);
            this.StylesTopPanel.Controls.Add(this.label5);
            this.StylesTopPanel.Controls.Add(this.txtStylesDescription);
            this.StylesTopPanel.Controls.Add(this.cmbStyles);
            this.StylesTopPanel.Controls.Add(this.label4);
            this.StylesTopPanel.Controls.Add(this.label3);
            this.StylesTopPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StylesTopPanel.Location = new System.Drawing.Point(3, 3);
            this.StylesTopPanel.Name = "StylesTopPanel";
            this.StylesTopPanel.Size = new System.Drawing.Size(599, 316);
            this.StylesTopPanel.TabIndex = 1;
            // 
            // PreviewBox
            // 
            this.PreviewBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PreviewBox.Location = new System.Drawing.Point(172, 69);
            this.PreviewBox.Name = "PreviewBox";
            this.PreviewBox.Size = new System.Drawing.Size(314, 235);
            this.PreviewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PreviewBox.TabIndex = 5;
            this.PreviewBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Preview:";
            // 
            // txtStylesDescription
            // 
            this.txtStylesDescription.Location = new System.Drawing.Point(318, 8);
            this.txtStylesDescription.Multiline = true;
            this.txtStylesDescription.Name = "txtStylesDescription";
            this.txtStylesDescription.ReadOnly = true;
            this.txtStylesDescription.Size = new System.Drawing.Size(234, 54);
            this.txtStylesDescription.TabIndex = 3;
            // 
            // cmbStyles
            // 
            this.cmbStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStyles.FormattingEnabled = true;
            this.cmbStyles.Location = new System.Drawing.Point(70, 21);
            this.cmbStyles.Name = "cmbStyles";
            this.cmbStyles.Size = new System.Drawing.Size(142, 21);
            this.cmbStyles.TabIndex = 8;
            this.toolTip.SetToolTip(this.cmbStyles, "Select a style for your album");
            this.cmbStyles.SelectedIndexChanged += new System.EventHandler(this.cmbStyles_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(229, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Description:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Style:";
            // 
            // StylesBottomPanel
            // 
            this.StylesBottomPanel.Controls.Add(this.btnSelectImages);
            this.StylesBottomPanel.Controls.Add(this.btnGenerateAlbum);
            this.StylesBottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.StylesBottomPanel.Location = new System.Drawing.Point(3, 319);
            this.StylesBottomPanel.Name = "StylesBottomPanel";
            this.StylesBottomPanel.Size = new System.Drawing.Size(599, 36);
            this.StylesBottomPanel.TabIndex = 0;
            // 
            // btnSelectImages
            // 
            this.btnSelectImages.Location = new System.Drawing.Point(11, 6);
            this.btnSelectImages.Name = "btnSelectImages";
            this.btnSelectImages.Size = new System.Drawing.Size(109, 25);
            this.btnSelectImages.TabIndex = 9;
            this.btnSelectImages.Text = "<< Select &Images";
            this.toolTip.SetToolTip(this.btnSelectImages, "Add more images");
            this.btnSelectImages.UseVisualStyleBackColor = true;
            this.btnSelectImages.Click += new System.EventHandler(this.btnSelectImages_Click);
            // 
            // btnGenerateAlbum
            // 
            this.btnGenerateAlbum.Location = new System.Drawing.Point(485, 6);
            this.btnGenerateAlbum.Name = "btnGenerateAlbum";
            this.btnGenerateAlbum.Size = new System.Drawing.Size(109, 25);
            this.btnGenerateAlbum.TabIndex = 10;
            this.btnGenerateAlbum.Text = "&Generate Album";
            this.toolTip.SetToolTip(this.btnGenerateAlbum, "Generate your album");
            this.btnGenerateAlbum.UseVisualStyleBackColor = true;
            this.btnGenerateAlbum.Click += new System.EventHandler(this.btnGenerateAlbum_Click);
            // 
            // helpProvider
            // 
            this.helpProvider.HelpNamespace = "fow.chm";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 455);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fotos on the Web";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabFOW.ResumeLayout(false);
            this.tabImages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImagesList)).EndInit();
            this.ImagesBottomPanel.ResumeLayout(false);
            this.ImagesTopPanel.ResumeLayout(false);
            this.ImagesTopPanel.PerformLayout();
            this.tabStyles.ResumeLayout(false);
            this.StylesTopPanel.ResumeLayout(false);
            this.StylesTopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PreviewBox)).EndInit();
            this.StylesBottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton tsNew;
        private System.Windows.Forms.ToolStripButton tsOpen;
        private System.Windows.Forms.ToolStripButton tsSave;
        private System.Windows.Forms.ToolStripButton tsHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsAbout;
        private System.Windows.Forms.ToolStripButton tsExit;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stylesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabFOW;
        private System.Windows.Forms.TabPage tabImages;
        private System.Windows.Forms.TabPage tabStyles;
        private System.Windows.Forms.ToolStripButton tsGenerate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Panel ImagesBottomPanel;
        private System.Windows.Forms.Panel ImagesTopPanel;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Button btnAddFolder;
        private System.Windows.Forms.Button btnAddImage;
        private System.Windows.Forms.TextBox txtAlbumDesc;
        private System.Windows.Forms.TextBox txtAlbumTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView ImagesList;
        private System.Windows.Forms.Button btnSelectStylesTab;
        private System.Windows.Forms.Panel StylesTopPanel;
        private System.Windows.Forms.Panel StylesBottomPanel;
        private System.Windows.Forms.Button btnSelectImages;
        private System.Windows.Forms.Button btnGenerateAlbum;
        private System.Windows.Forms.PictureBox PreviewBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStylesDescription;
        private System.Windows.Forms.ComboBox cmbStyles;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.DataGridViewImageColumn colImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn colImgtitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn colImgTxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFullPathWithExt;
        private System.Windows.Forms.HelpProvider helpProvider;
    }
}

