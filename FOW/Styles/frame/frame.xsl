<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
Frame style album
Using CSS, we emulate the frame style
On the left side a column of thumbnails are displayed
On the right side the correct image is displayed when the user clicks on one thumbnail
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- 	This is the only style with xhtml -->
  <xsl:output method="html" version="1.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:template match="/">
    <html>
      <head>
        <meta name="generator" content="Fotos on web (www.jjude.com/apps/fow.html)"/>
        <title>
          <xsl:value-of select="Album/Title"/>
        </title>
        <!-- This style is specific to this layout -->
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <script language="JavaScript" type="text/javascript">
          <xsl:comment>
            <!-- This is the array of image path -->
            <xsl:text>
              Imgs = new Array( 
            </xsl:text>
            <xsl:for-each select="Album/Images/Img">
              <xsl:text>"</xsl:text>
              <xsl:value-of select="translate(ImgPath, '\', '/')"/>
              <xsl:text>"</xsl:text>
              <xsl:if test="not (position() = last())"> ,</xsl:if>
            </xsl:for-each>
            <xsl:text>);</xsl:text>
            <!-- This is the array of titles  -->
            <xsl:text>Titles = new Array( </xsl:text>
            <xsl:for-each select="Album/Images/Img">
              <xsl:text>"</xsl:text>
              <xsl:value-of select="ImgTitle"/>
              <xsl:text>"</xsl:text>
              <xsl:if test="not (position() = last())"> ,</xsl:if>
            </xsl:for-each>
            <xsl:text>);</xsl:text>
            <!-- This is the array of descriptions  -->
            <xsl:text>Descriptions = new Array( </xsl:text>
            <xsl:for-each select="Album/Images/Img">
              <xsl:text>"</xsl:text>
              <xsl:value-of select="ImgDesc"/>
              <xsl:text>"</xsl:text>
              <xsl:if test="not (position() = last())"> ,</xsl:if>
            </xsl:for-each>
            <xsl:text>);</xsl:text>
            <xsl:text>
							<!-- This function will display the image on the right side -->				
						function displayimage(ImgNumber){ 
						var ImgToDisplay = ImgNumber;
						 //javascript arrays start from 0 
						 document.slideshow.src = Imgs[ImgToDisplay];
						 } 
						 </xsl:text>
            // End
          </xsl:comment>
        </script>
      </head>
      <body class="ImgPgBody">
        <!-- Set the album title-->
        <h1 class="navigation">
          <xsl:value-of select="Album/Title"/>
        </h1>
        <!-- Set the album description-->
        <p class = "AlbumDesc">
          <xsl:value-of select="Album/Description"/>
        </p>
        <br/>
        <!-- Set the thumbnails on the left-->
        <div class="LeftBlock">
          <xsl:for-each select="Album/Images/Img">
            <a href="javascript:displayimage({ImgId})">
              <xsl:element name="img">
                <xsl:attribute name="src">
                  <xsl:value-of select="ThumbPath"/>
                </xsl:attribute>
                <xsl:attribute name="alt">
                  <xsl:value-of select="ImgTitle"/>
                </xsl:attribute>
                <xsl:attribute name="border">0</xsl:attribute>
              </xsl:element>
            </a>
            <BR/>
            <BR/>
          </xsl:for-each>
        </div>
        <div class="PictureDisplay">
          <xsl:element name="img">
            <xsl:attribute name="src">
              <xsl:value-of select="Album/Images/Img[1]/ImgPath"/>
            </xsl:attribute>
            <xsl:attribute name="alt">Image</xsl:attribute>
            <xsl:attribute name="name">slideshow</xsl:attribute>
          </xsl:element>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
