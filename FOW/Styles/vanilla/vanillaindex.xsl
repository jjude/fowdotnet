<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
Vanilla Index page stylesheet
This creates a html table with thumbnails on the left with the descriptions on the right
It links the thumbnails to the image
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.0" encoding="ISO-8859-1" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta name="generator" content="Fotos on web (http://apps.jjude.com)"/>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta http-equiv="content-type" name="keywords" content="photos, fow, fotos on web" />
        <title>
          <xsl:value-of select="Album/Title"/>
        </title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
      </head>
      <body>
        <!-- Set the album title-->
        <h1>
          <xsl:value-of select="Album/Title"/>
        </h1>
        <!-- Set the album description-->
        <p class = "AlbumDesc">
          <xsl:value-of select="Album/Description"/>
        </p>
        <div id="page">
          <!-- Now create table with the thumbnails and descriptions -->
          <table border="0" cellspacing ="2"  class ="ThumbTable">
            <xsl:for-each select="Album/Images/Img">
              <tr>
                <td class ="ThumbCell">
                  <a href="{ImgPgURL}">
                    <xsl:element name="img">
                      <xsl:attribute name="src">
                        <xsl:value-of select="ThumbPath"/>
                      </xsl:attribute>
                      <xsl:attribute name="alt">
                        <xsl:value-of select="ImgTitle"/>
                      </xsl:attribute>
                    </xsl:element>
                  </a>
                  <BR/>
                </td>
                <td class ="ThumbCell">
                  <span class ="ThumbText">
                    <xsl:value-of select="ImgDesc"/>
                  </span>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </div>
        <div id="footer">
          Album generated with <a href="http://code.google.com/p/fowdotnet/" alt="fowdotnet">FOW</a>
          <!--mention whatever you want to mention in the footer -->
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
