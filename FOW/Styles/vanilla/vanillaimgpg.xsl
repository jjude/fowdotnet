<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
	Simple Image page style
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.0" encoding="ISO-8859-1" indent="yes"/>
  <!-- we pass ImgId as a parameter from the calling program  -->
  <xsl:param name="ImgId"></xsl:param>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta name="generator" content="Fotos on web (http://apps.jjude.com)"/>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta http-equiv="content-type" name="keywords" content="photos, fow, fotos on web" />
        <title>
          <xsl:value-of select="Album/Title"/>
        </title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
      </head>
      <body>
        <!-- Set album title -->
        <h1 class="navigation">
          <xsl:value-of select="Album/Title"/> | <xsl:value-of select="Album/Images/Img[position()=$ImgId]/ImgTitle"/>
        </h1>
        <xsl:apply-templates select="/Album/Images/Img[position()=$ImgId]"/>
        <div id="footer">
          Album generated with <a href="http://code.google.com/p/fowdotnet/" alt="fowdotnet">FOW</a>
          <!--mention whatever you want to mention in the footer -->
        </div>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="Img">
    <div class="navigation">
      <div class="menu">
      <xsl:choose>
        <xsl:when test="$ImgId = 1">
          First | Prev |
          <a href="{/Album/Images/Img[$ImgId + 1]/ImgPgURL}">Next</a>|
          <a href="{/Album/Images/Img[last()]/ImgPgURL}">Last</a>|
          <a href="{/Album/IndexPage}">Index</a>
        </xsl:when>
        <xsl:when test="$ImgId = string(/Album/TotalImages)">
          <a href="{/Album/Images/Img[1]/ImgPgURL}">First</a> |
          <a href="{/Album/Images/Img[$ImgId - 1]/ImgPgURL}"> Prev</a>| Next | Last|
          <a href="{/Album/IndexPage}">Index</a>
        </xsl:when>
        <xsl:otherwise>
          <a href="{/Album/Images/Img[1]/ImgPgURL}">First</a> |
          <a href="{/Album/Images/Img[$ImgId - 1]/ImgPgURL}">Prev</a>|
          <a href="{/Album/Images/Img[$ImgId + 1]/ImgPgURL}">Next</a>|
          <a href="{/Album/Images/Img[last()]/ImgPgURL}">Last</a>|
          <a href="{/Album/IndexPage}">Index</a>
        </xsl:otherwise>
      </xsl:choose>
    </div>
    </div>
    <p id="separator"></p>
    <img src="{translate(ImgPath, '\', '/')}" alt="ImgTitle"/>
    <p>
      <xsl:value-of select="ImgDesc"/>
    </p>

  </xsl:template>
</xsl:stylesheet>
