<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
Frame style album
Using CSS, we emulate the frame style
On the left side a column of thumbnails are displayed
On the right side the correct image is displayed when the user clicks on one thumbnail
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- 	This is the only style with xhtml -->
  <xsl:output method="xml" version="1.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:template match="/">
    <xsl:element name="simpleviewerGallery">
      <xsl:attribute name="maxImageWidth">480</xsl:attribute>
      <xsl:attribute name="maxImageHeight">480</xsl:attribute>
      <xsl:attribute name="textColor">0xFFFFFF</xsl:attribute>
      <xsl:attribute name="frameColor">0xffffff</xsl:attribute>
      <xsl:attribute name="frameWidth">20</xsl:attribute>
      <xsl:attribute name="stagePadding">40</xsl:attribute>
      <xsl:attribute name="thumbnailColumns">3</xsl:attribute>
      <xsl:attribute name="thumbnailRows">3</xsl:attribute>
      <xsl:attribute name="navPosition">left</xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="Album/Title"/>
      </xsl:attribute>
      <xsl:attribute name="enableRightClickOpen">false</xsl:attribute>
      <xsl:attribute name="backgroundImagePath"></xsl:attribute>
      <xsl:attribute name="imagePath">images/</xsl:attribute>
      <xsl:attribute name="thumbPath">thumbs/</xsl:attribute>
      <xsl:for-each select="Album/Images/Img">
        <xsl:element name="image">
          <xsl:element name="filename">
            <xsl:value-of select="ImgName"/>
          </xsl:element>
          <xsl:element name="caption">
            <xsl:value-of select="ImgTitle"/>
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
