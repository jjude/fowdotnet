using System;
using System.Collections.Generic;
using System.Windows.Forms;
//Added by Joseph
using System.Collections; //For ArrayList
using System.IO; //for directory
//TODO: enable theming; search on visualstyles
using System.Windows.Forms.VisualStyles;

namespace FOW
{
    //in oops there is no concept of global vars
    //so define a static class and use it
    //since it is static it doesnt have to be instantiated
    static class GlobalClass
    {
        //struct to hold different options
        public struct Options
        {
            private string m_displayName;
            private string m_description;
            private string m_mainstyle;
            private string m_imgstyle;//if there should be one HTML per image then this holds the xsl for the images
            private string m_onlyindex; //is it a single output or one for each image?
            private string m_previewimg; //preview of the style
            private string m_styledir; //directory name of this style
            private string m_outputfilename; //by default it is index.html; but may be something else

            public Options(string DispName, string Desc, string MainXSL, string ImgXSL, string OnlyIndex, string PreviewImage, string StyleDir, string OutputFileName)
            {
                m_displayName = DispName;
                m_description = Desc;
                m_mainstyle = MainXSL;
                m_imgstyle = ImgXSL;
                m_onlyindex = OnlyIndex;
                m_previewimg = PreviewImage;
                m_styledir = StyleDir;
                m_outputfilename = OutputFileName;
            }

            public string Description
            {
                get { return m_description; }
                set { m_description = value; }
            }

            public string MainStyle
            {
                get { return m_mainstyle; }
                set { m_mainstyle = value; }
            }

            public string ImgStyle
            {
                get { return m_imgstyle; }
                set { m_imgstyle = value; }
            }

            public string IsOnlyIndex
            {
                get { return m_onlyindex; }
                set { m_onlyindex = value; }
            }

            public string PreviewImg
            {
                get { return m_previewimg; }
                set { m_previewimg = value; }
            }

            public string StyleDir
            {
                get { return m_styledir; }
                set { m_styledir = value; }
            }

            public string OutputFileName
            {
                get { return m_outputfilename; }
                set { m_outputfilename = value; }
            }
        }

        //public array to hold styles
        public static ArrayList StylesOptions = new ArrayList();
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.VisualStyleState = System.Windows.Forms.VisualStyles.VisualStyleState.ClientAndNonClientAreasEnabled;
            //if styles dir doesnt exist, then quit
            if (!Directory.Exists("Styles"))
            {
                MessageBox.Show("There are no styles defined. FOW is not installed properly. Please install again", "No Styles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Application.Run(new frmMain());
        }
    }
}