FOW is a simple to use photo album generator.

Generating the album is easy:

* Add your images
* Select a style
* Generate the album
If you want, you can add album title, album description and title and description for each of the image.

A detailed help file is provided with the application. Below is help text just for installation.
*****
## Installation ##
### Using Setup files ###
* Locate the folder in which the setup package is stored.
* Double-click FOWSetup.msi.
* When FOW InstallShield? Wizard appears, click Next.
* In the 'Select Installation Folder', either accept the installation folder, or change to another folder by clicking on 'Browse'.
* Click 'Next' to install FOW
## Using Zip file ## 
(This assumes that you already have .NET 2.0 installed in your system)

* Locate the folder in which the zip file is downloaded.
* Unzip the contents into a folder

## To Uninstall FOW ##
* If you've installed via setup files, then uninstall via 'Add/Remove Programs'.
* If you've installed via zip files, just delete the folder that contains FOW.